public class S2 {
    static class Display extends Thread {

        public Display(String name){
            super(name);
        }
         void print(){
            System.out.println(Thread.currentThread().getName()+":"+System.currentTimeMillis());
        }
        @Override
        public void run() {
            synchronized (this) {
                for (int i = 0; i < 2; i++){


                    try {
                        print();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }

    }

    public static void main(String[] args) {

        Display t1 = new Display("KThread1");
        Display t2 = new Display("KThread2");
        Display t3 = new Display("KThread3");
        t1.start();
        t2.start();
        t3.start();

    }

}
